" ------------------------------------------------------------------
" Solarized Colorscheme Config
" ------------------------------------------------------------------
" syntax enable
set background=dark
colorscheme solarized
" ------------------------------------------------------------------

" The following items are available options, but do not need to be
" included in your .vimrc as they are currently set to their defaults.

" let g:solarized_termtrans=0
" let g:solarized_degrade=0
" let g:solarized_bold=1
" let g:solarized_underline=1
" let g:solarized_italic=1
let g:solarized_termcolors=256
" let g:solarized_contrast="normal"
" let g:solarized_visibility="normal"
" let g:solarized_diffmode="normal"
" let g:solarized_hitrail=0
" let g:solarized_menu=1

set guifont=Source\ Code\ Pro\ for\ Powerline:h14            " Vim界面使用的字体
set guifontwide=Lantinghei\ SC\ Demibold:h14
set guioptions-=m  "no menu
set guioptions-=T  "no toolbar
set guioptions-=l
set guioptions-=L
set guioptions-=r  "no scrollbar
set guioptions-=R


